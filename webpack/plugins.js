const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const SplitByPathPlugin = require('webpack-split-by-path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const env = (process.env.NODE_ENV === 'staging') ? JSON.stringify('production') : JSON.stringify(process.env.NODE_ENV) ;
const basePlugins = [
  new webpack.DefinePlugin({
    __DEV__: process.env.NODE_ENV === 'development',
    'process.env.NODE_ENV': env,
  }),
  new SplitByPathPlugin([
    { name: 'vendor', path: [__dirname + '/node_modules/'] },
  ]),
  new HtmlWebpackPlugin({
    template: './src/index.html',
    inject: 'body',
    title: 'React training project',
    favicon: './src/assets/favicon.ico'
  }),
  new webpack.NoErrorsPlugin(),
  new CopyWebpackPlugin([
    { from: 'src/assets', to: 'assets' },
  ]),
];
const prodPlugins = [
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      warnings: false,
    },
  }),
];
const devPlugins = [
  new webpack.HotModuleReplacementPlugin(),
];

module.exports = basePlugins
  .concat(process.env.NODE_ENV === 'production' ? prodPlugins : [])
  .concat(process.env.NODE_ENV === 'development' ? devPlugins : []);
