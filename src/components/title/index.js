import React from 'react';

function Title ({children}) {
  return(
      <h1>
            {children}
      </h1>
);}


Title.propTypes = {
  children: React.PropTypes.node,
};

export default Title;
